#!/usr/bin/env bash
cd $(dirname $0)
set -eE
trap "exit" INT

jq -s add ../projects/*.json | ./generate.coffee -j -
