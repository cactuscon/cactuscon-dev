#!/usr/bin/env bash
eval $(docker-machine env --shell bash cactuscon-dev)
IP=$(docker-machine ip cactuscon-dev)
HOST=127.0.0.1,$(docker-machine ip cactuscon-dev | sed -e 's|\.[0-9]*$|.1|')
./setup.sh $IP $HOST
pkill dnsmasq
echo + dnsmasq
