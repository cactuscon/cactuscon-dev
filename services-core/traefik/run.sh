#!/usr/bin/env bash
eval $(docker-machine env --shell bash cactuscon-dev)
CACTUSCON_DEV_IP=$(docker-machine ip cactuscon-dev)

sed -e "s|{{IP}}|${CACTUSCON_DEV_IP}|" \
  <traefik.toml.template >traefik.toml

echo + Træfɪk
docker-compose up -d
